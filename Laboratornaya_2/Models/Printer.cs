﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Laboratornaya_2.Models
{
    public static class Printer
    {
        private static ConcurrentDictionary<Guid, string> _filesToPrint = new ConcurrentDictionary<Guid, string>();
        private static Random random;
        public static Action<string> PrintFn;

        public async static void TakeFileToPrint(PC pc)
        {
            await Task.Run(() =>
            {
                int operationDuration;
                Guid guid;
                if (random == null)
                    random = new Random();
                operationDuration = random.Next(1000, 2000);

                PrintFn?.Invoke(string.Format("Начата операция принятия в печать файла {0}, длительность - {1}" + Environment.NewLine, pc.File, operationDuration));
                Thread.Sleep(operationDuration);
                guid = Guid.NewGuid();
                _filesToPrint.TryAdd(guid, pc.File);
                PrintFn?.Invoke(string.Format("Закончена операция принятия в печать файла {0}, длительность - {1}" + Environment.NewLine, pc.File, operationDuration));


                var fileToOutput = string.Empty;

                if (_filesToPrint.TryRemove(guid, out fileToOutput))
                {
                    Thread.Sleep(1000);
                    PrintFn?.Invoke(string.Format("Файл напечатан - {0}" + Environment.NewLine, fileToOutput));
                }
                else
                {
                    PrintFn?.Invoke("Какая-то ошибка. Ну это ж принтер.");
                }
            });
        }

        private static void PrintToOutput(string text)
        {
            PrintFn?.Invoke(text);
        }

        private static void PrintFile(string file)
        {
            PrintFn?.Invoke(string.Format("Напечатал файл - {0}", file));
        }
    }
}
